// CONFIGURAÇÃO
// =============================================================================

// BIBLIOTECAS
var express    = require('express');        
var app        = express();                
var bodyParser = require('body-parser');
var morgan      = require('morgan');

//MODELS
var Usuario     = require('./app/models/Usuario');

//DATABASE
var mongoose   = require('mongoose');
var configDB = require('./config/database.js');
mongoose.connect(configDB.url);

//HABILITA POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//PORTA DA APLICAÇÃO
var port = process.env.PORT || 8080;        

// GERAR LOG DE REQUESTS NO CONSOLE
app.use(morgan('dev'));

//ROTAS
var apiRouter = require('express').Router();
require('./app/routes/index.js')(apiRouter, bodyParser)
app.use('/api', apiRouter);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);