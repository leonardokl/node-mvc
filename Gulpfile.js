var gulp = require('gulp');
var traceur = require('gulp-traceur-compiler');
 
gulp.task('bundle', function () {
    return gulp.src('./app/models/es6/**/*.js')
        .pipe(traceur())
        .pipe(gulp.dest('./app/models/'));
});