//MODELS
var Usuario               = require('../models/Usuario');
var UsuarioController     = require('../controllers/UsuarioController');

module.exports = function(router,bodyParser){

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

//inicio
router.get('/', function(req, res) {   
    res.json({ message: 'NODE - REST API' }); 
});

//TESTES
router.get('/test', function(req, res) {   
    res.json({ message: 'NODE - REST ee' }); 
});

router.route('/usuarios')

    .post(function(req, res) {
        
        var usuario = new Usuario();      
        usuario.nome = req.body.nome; 

        // salva o usuário e confere se há erros
        usuario.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });
        
    })
   
    .get(function(req, res) {
        Usuario.find(function(err, usuarios) {
            if (err)
                res.send(err);

            res.json(usuarios);
        });
    });

router.route('/usuarios/:usuario_id')

    .get(function(req, res) {
        Usuario.findById(req.params.usuario_id, function(err, usuario) {
            if (err)
                res.send(err);
            res.json(usuario);
        });
    })

    .put(function(req, res) {        
        Usuario.findById(req.params.usuario_id, function(err, usuario) {

            if (err)
                res.send(err);

            usuario.nome = req.body.nome;  // atuliza as informações do objeto

            // salva o usuário
            usuario.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });
        });
    })

    .delete(function(req, res) {
        Usuario.remove({
            _id: req.params.usuario_id
        }, function(err, usuario) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });
}