var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var bcrypt   	 = require('bcrypt-nodejs');

var UsuarioSchema   = new Schema({
    nome:  String,
    senha: String
});

// methods ======================
// gerar hash da senha
UsuarioSchema.methods.gerarHash = function(senha) {
    return bcrypt.hashSync(senha, bcrypt.genSaltSync(8), null);
};

// conferir senha
UsuarioSchema.methods.validarSenha = function(senha) {
    return bcrypt.compareSync(senha, this.senha);
};

module.exports = mongoose.model('Usuario', UsuarioSchema);