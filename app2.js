// CONFIGURAÇÃO
// =============================================================================

// BIBLIOTECAS
var express    = require('express');        
var app        = express();                
var bodyParser = require('body-parser');
var morgan      = require('morgan');

//MODELS
var Usuario     = require('./app/models/Usuario');

//DATABASE
var mongoose   = require('mongoose');
var configDB = require('./config/database.js');
mongoose.connect(configDB.url);

//HABILITA POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//PORTA DA APLICAÇÃO
var port = process.env.PORT || 8080;        

// GERAR LOG DE REQUESTS NO CONSOLE
app.use(morgan('dev'));


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

router.route('/usuarios')

    .post(function(req, res) {
        
        var usuario = new Usuario();      // create a new instance of the Usuario model
        usuario.nome = req.body.nome;  // set the request

        // alva o usuário e conferi se há erros
        usuario.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'User created!' });
        });
        
    })
   
    .get(function(req, res) {
        Usuario.find(function(err, usuarios) {
            if (err)
                res.send(err);

            res.json(usuarios);
        });
    });

router.route('/usuarios/:usuario_id')

    .get(function(req, res) {
        Usuario.findById(req.params.usuario_id, function(err, usuario) {
            if (err)
                res.send(err);
            res.json(usuario);
        });
    })

    .put(function(req, res) {        
        Usuario.findById(req.params.usuario_id, function(err, usuario) {

            if (err)
                res.send(err);

            usuario.nome = req.body.nome;  // atuliza as informações do objeto

            // salva o usuário
            usuario.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'User updated!' });
            });
        });
    })

    .delete(function(req, res) {
        Usuario.remove({
            _id: req.params.usuario_id
        }, function(err, usuario) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });



//GET /
router.get('/', function(req, res) {   
    res.json({ message: 'NODE - REST API' }); 
});


// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);